package com.nurse.nurse.service;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.nurse.nurse.model.RegistrionDto;

@Repository("userRepository")
public interface UserRepository extends JpaRepository<RegistrionDto, Long> {
 
 RegistrionDto findByEmail(String email);
}