package com.nurse.nurse.service;

import com.nurse.nurse.model.RegistrionDto;

public interface UserService {
	  
	 public RegistrionDto findUserByEmail(String email);
	 
	 public void saveUser(RegistrionDto user);
	}
