package com.nurse.nurse.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.nurse.nurse.model.RegistrionDto;


@Service("userService")
public class UserServiceImpl implements UserService {
 
 @Autowired
 private UserRepository userRepository;
 

 
 @Autowired
 private BCryptPasswordEncoder bCryptPasswordEncoder;

 @Override
 public RegistrionDto findUserByEmail(String email) {
  return userRepository.findByEmail(email);
 }

 @Override
 public void saveUser(RegistrionDto user) {
	 
	 System.out.println("====>"+user.getPassword()+user.toString());
  user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
 
  userRepository.save(user);
 }



}