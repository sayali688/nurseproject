package com.nurse.nurse.config;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;


@RestController
@CrossOrigin
public class JwtAuthenticationController {

	

	@Autowired
	private JwtTokenUtil jwtTokenUtil;

	@Autowired
	private JwtUserDetailsService userDetailsService;

	//@Autowired
	//private BCryptPasswordEncoder bCryptPasswordEncoder;
	
	
	
	@PostMapping(path = "/authenticate" ,consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest)  {

		String token="";
		try {
			if(authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword())) {
				final UserDetails userDetails = userDetailsService
						.loadUserByUsername(authenticationRequest.getUsername());

				  token = jwtTokenUtil.generateToken(userDetails);   

				
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return ResponseEntity.ok("Invalid User");
		}

		return ResponseEntity.ok(new JwtResponse(token));	
	}

	private boolean authenticate(String username, String password) throws Exception {
		try {
			
			final UserDetails userDetails = userDetailsService
					.loadUserByUsername(username);
			if(null ==userDetails)
				 throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
			
			/*if (!bCryptPasswordEncoder.matches(password, userDetails.getPassword())) {
	            throw new BadCredentialsException("Authentication Failed. Username or Password not valid.");
	        }*/
			//authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, bCryptPasswordEncoder.encode(password)));
		} catch (DisabledException e) {
			e.printStackTrace();
			throw new Exception("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			e.printStackTrace();
			throw new Exception("INVALID_CREDENTIALS", e);
		}
		
		return true;
	}
	

	
	
	
}
