package com.nurse.nurse.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="RegistrionDto")
public class RegistrionDto {
	    
	    @Id
	    @GeneratedValue(strategy = GenerationType.AUTO)
	    private int id;
	    
	    @Column(name = "email")
	    private String email;
	    
	    @Column(name = "firstname")
	    private String firstname; 
	    
	    @Column(name = "lastname")
	    private String lastname;
	    
	    @Column(name = "password")
	    private String password;
	    
	
	    @Column(name = "conpassword")
	    private String conpassword;
	    
	    
	    
	    public int getId() {
	    	  return id;
	    	 }

	    	 public void setId(int id) {
	    	  this.id = id;
	    	 }

	    	 public String getEmail() {
	    	  return email;
	    	 }

	    	 public void setEmail(String email) {
	    	  this.email = email;
	    	 }

	    	 public String getFirstname() {
	    	  return firstname;
	    	 }

	    	 public void setFirstname(String firstname) {
	    	  this.firstname = firstname;
	    	 }

	    	 public String getLastname() {
	    	  return lastname;
	    	 }

	    	 public void setLastname(String lastname) {
	    	  this.lastname = lastname;
	    	 }

	    	 public String getPassword() {
	    	  return password;
	    	 }

	    	 public void setPassword(String password) {
	    	  this.password = password;
	    	 }

			public String getConpassword() {
				return conpassword;
			}

			public void setConpassword(String conpassword) {
				this.conpassword = conpassword;
			}

			@Override
			public String toString() {
				return "RegistrionDto [id=" + id + ", email=" + email + ", firstname=" + firstname + ", lastname="
						+ lastname + ", password=" + password + ", conpassword=" + conpassword + "]";
			}

			public RegistrionDto() {
				super();
			}

	    	
	    
	 	
}
